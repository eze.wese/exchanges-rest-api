# Exchanges API

This REST API integrates with crypto exchanges and exposes aggregated data for visualization in an external application. 
The REST API implementation was developed with [Hexagonal Architecture](https://blog.octo.com/hexagonal-architecture-three-principles-and-an-implementation-example).
## Requirements

1. Java 17.x.x
2. Gradle 7.4.2
3. MongoDB

## MongoDB Installation

- The installation of MongoDB is a requirement to run the project correctly. So, in the root path of the project there is a docker compose [file](docker-compose.yaml) to install locally MongoDB. For this at the project root path execute:
```bash
docker compose up -d
```

## Steps to Setup

**1. Clone the application**

```bash
git clone https://gitlab.com/eze.wese/exchanges-rest-api.git
```

**2. Build and run the application app using gradle**

```bash
cd exchange-rest-api
SPRING_PROFILES_ACTIVE=local gradle clean bootRun -DSCOPE=local
```

## Swagger

- Swagger is already configured in this project.
- The Swagger UI can be seen at http://localhost:8080/swagger-ui/index.html
- The Open API documentation v3 is at http://localhost:8080/v3/api-docs.


## Note

- [Example](metadata-test.csv) of csv file to attach in POST `/metadata`

## Next Steps

- Add Unit Tests
- Performance and optimisation, particularly in `GET /order-books` endpoint. Takes a long period of time to respond at first time.
- Pagination
- Finish with the Dockerization