package com.exchanges.adapters.ordersBook.out;

import com.exchanges.apis.BlockchainApi;
import com.exchanges.application.port.out.RetrieveSymbolsPort;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
@Component
public class RetrieveSymbolsAdapter implements RetrieveSymbolsPort {

    private final BlockchainApi blockchainApi;
    private final Cache<String, List<String>> symbolsCache = Caffeine.newBuilder().expireAfterWrite(12, TimeUnit.HOURS).maximumSize(1).build();

    @Override
    public List<String> retrieveSymbols() {
        var cachedSymbols = symbolsCache.getIfPresent("Symbols");
        if (isNull(cachedSymbols)) {
            var symbols = blockchainApi.retrieveSymbols().keySet().stream().toList();
            symbolsCache.put("Symbols", symbols);
            return symbols;
        }
        return cachedSymbols;
    }
}
