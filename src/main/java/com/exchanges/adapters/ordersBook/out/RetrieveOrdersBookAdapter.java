package com.exchanges.adapters.ordersBook.out;

import com.exchanges.apis.BlockchainApi;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.exchanges.application.port.out.RetrieveOrdersBookDataPort;
import com.exchanges.domain.ordersBook.OrderBooks;
import com.exchanges.domain.ordersBook.OrderBooks.OrderTypeDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Objects.isNull;

@RequiredArgsConstructor
@Component
public class RetrieveOrdersBookAdapter implements RetrieveOrdersBookDataPort {

    private final BlockchainApi blockchainApi;
    private final Cache<String, List<OrderBooks>> ordersBookCache = Caffeine.newBuilder().expireAfterWrite(6, TimeUnit.HOURS).maximumSize(1).build();
    private static final DecimalFormat df = new DecimalFormat("0.00");
    @Override
    public List<OrderBooks> retrieveOrdersBook(List<String> symbols) {
        var cachedOrdersBook = ordersBookCache.getIfPresent("OrdersBook");

        if (isNull(cachedOrdersBook)) {
            var ordersBook = new ArrayList<OrderBooks>();
            symbols.forEach(symbol -> {
                var apiResponse = blockchainApi.retrieveOrderBook(symbol);
                AtomicReference<Double> bidPxTotal = new AtomicReference<>(0.0);
                AtomicReference<Double> bidQtyTotal = new AtomicReference<>(0.0);
                AtomicReference<Double> askPxTotal = new AtomicReference<>(0.0);
                AtomicReference<Double> askQtyTotal = new AtomicReference<>(0.0);
                apiResponse.bids().forEach(bid -> {
                    bidPxTotal.updateAndGet(v -> v + bid.px());
                    bidQtyTotal.updateAndGet(v -> v + bid.qty());
                });
                apiResponse.asks().forEach(ask -> {
                    askPxTotal.updateAndGet(v -> v + ask.px());
                    askQtyTotal.updateAndGet(v -> v + ask.qty());
                });
                var bidSize = new BigDecimal(String.valueOf(apiResponse.bids().size()));
                var askSize = new BigDecimal(String.valueOf(apiResponse.asks().size()));
                var mapValues = Map.of(
                        "bid", new OrderTypeDetail(df.format(bidPxTotal.get() / apiResponse.bids().size()), df.format(bidQtyTotal.get() / apiResponse.bids().size())),
                        "ask", new OrderTypeDetail(df.format(askPxTotal.get() / apiResponse.asks().size()), df.format(askQtyTotal.get() / apiResponse.asks().size())));
                ordersBook.add(new OrderBooks(symbol, mapValues));
            });
            ordersBookCache.put("OrdersBook", ordersBook);
            return ordersBook;
        }

        return cachedOrdersBook;
    }
}
