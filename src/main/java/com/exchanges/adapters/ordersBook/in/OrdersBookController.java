package com.exchanges.adapters.ordersBook.in;

import com.exchanges.application.port.in.ProvideOrdersBookDataUseCase;
import com.exchanges.domain.ordersBook.OrderBooks;
import com.exchanges.domain.ordersBook.OrdersBookFilter;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("exchanges")
@RequiredArgsConstructor
public class OrdersBookController {

    private final ProvideOrdersBookDataUseCase provideOrdersBookDataUseCase;

    @Operation(summary = "Find order books", description = "Provides the quantity and price average of the order book (asks and bids) for\n" +
            "each symbol. Allows to filter by symbol and order type (ask/bid) and allows order by symbol alphabetically.")
    @GetMapping("/blockchain/order-books")
    public ResponseEntity<List<OrderBooks>> getOrderBooks(
            @RequestParam(required = false) String symbol,
            @RequestParam(required = false) String orderType,
            @RequestParam(required = false) Boolean orderAlphabetically
    ) {
        return ResponseEntity.ok(provideOrdersBookDataUseCase.retrieveOrdersBook(new OrdersBookFilter(symbol, orderType, orderAlphabetically)));
    }
}
