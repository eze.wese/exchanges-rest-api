package com.exchanges.adapters.metadata.in;

import com.exchanges.application.port.in.SaveMetadataUseCase;
import com.exchanges.common.CSVHelper;
import com.exchanges.domain.metadata.ResponseMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.exchanges.application.port.in.ProvideExchangeMetadataUseCase;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("exchanges")
@RequiredArgsConstructor
public class MetadataController {

    private final SaveMetadataUseCase saveMetadataUseCase;
    private final ProvideExchangeMetadataUseCase provideExchangeMetadataUseCase;

    @Operation(summary = "Upload a csv file with exchange metadata", description = "Allows upload the metadata for a particular exchange\n" +
            "through an internal backoffice.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "File uploaded successfully."),
            @ApiResponse(responseCode = "400", description = "Format file not valid, please upload a CSV file."),
            @ApiResponse(responseCode = "417", description = "The file could not be uploaded.") })
    @PostMapping(value = "/{exchange-name}/metadata", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<ResponseMessage> saveMetadata(
            @RequestParam("csv-file") @Parameter(description = "csv-file") MultipartFile file,
            @PathVariable(name = "exchange-name") String exchangeName) {
        if (!CSVHelper.hasCSVFormat(file)) return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage("Format file not valid, please upload a CSV file."));
        try {
            saveMetadataUseCase.saveMetadata(file, exchangeName);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("File uploaded successfully."));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("The file could not be uploaded."));
        }
    }

    @Operation(summary = "Get metadata for exchange", description = "Provides a group of key-value pairs with exchange metadata (for example name,\n" +
            "description, webpage, address, etc).")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, examples = {
                            @ExampleObject("{\n" +
                                    "    \"name\": \"blockchain\",\n" +
                                    "    \"description\": \"The world’s leading crypto...\",\n" +
                                    "    \"address\": \"John Street 565\",\n" +
                                    "    \"webpage\": \"https://www.blockchain.com/en/\"\n" +
                                    "}")
                    })
            }),
            @ApiResponse(responseCode = "404", description = "Metadata for exchange name sent doesn't exist.", content = @Content) })
    @GetMapping("/{exchange-name}/metadata")
    public ResponseEntity<String> retrieveMetadata(@PathVariable(name = "exchange-name") String exchangeName) throws JsonProcessingException {
        return ResponseEntity.ok(provideExchangeMetadataUseCase.retrieveExchangeMetadata(exchangeName));
    }
}
