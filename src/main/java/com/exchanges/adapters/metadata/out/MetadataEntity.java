package com.exchanges.adapters.metadata.out;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Data
@Document(collection = "metadata")
public class MetadataEntity {
    @Id
    private String id;
    private Map<String, Object> metadata;
}
