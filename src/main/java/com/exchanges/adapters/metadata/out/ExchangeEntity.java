package com.exchanges.adapters.metadata.out;

import com.exchanges.domain.metadata.Exchange;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "exchanges")
public class ExchangeEntity {
    @Id
    private String id;
    @Indexed(unique = true)
    private String name;

    public Exchange toExchangeModel() {
        return new Exchange(id, name);
    }
}
