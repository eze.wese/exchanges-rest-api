package com.exchanges.adapters.metadata.out;

import com.exchanges.application.port.out.SaveExchangeDataPort;
import com.exchanges.domain.metadata.Exchange;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Component
public class SaveExchangeDataAdapter implements SaveExchangeDataPort {

    private final ExchangesRepository exchangesRepository;

    @Override
    public Exchange saveExchangeData(String id, String name) {
        var exchangeOpt = exchangesRepository.findByName(name);
        if (exchangeOpt.isPresent()) return exchangeOpt.get().toExchangeModel();
        var exchangeEntity = new ExchangeEntity();
        exchangeEntity.setId(id);
        exchangeEntity.setName(name);

        return exchangesRepository.save(exchangeEntity).toExchangeModel();
    }
}
