package com.exchanges.adapters.metadata.out;

import com.exchanges.application.port.out.SaveMetadataPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Map;

@RequiredArgsConstructor
@Component
public class SaveMetadataAdapter implements SaveMetadataPort {

    private final MetadataRepository metadataRepository;

    @Override
    public void saveMetadata(String id, Map<String, Object> metadataToAdd) {
        var metaDataOpt = metadataRepository.findById(id);
        if (metaDataOpt.isPresent()) {
            var existingMetadataEntity = metaDataOpt.get();
            existingMetadataEntity.getMetadata().putAll(metadataToAdd);
            metadataRepository.save(existingMetadataEntity);
            return;
        }
        var metadataEntity = new MetadataEntity();
        metadataEntity.setId(id);
        metadataEntity.setMetadata(metadataToAdd);
        metadataRepository.save(metadataEntity);
    }
}
