package com.exchanges.adapters.metadata.out;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ExchangesRepository extends MongoRepository<ExchangeEntity, String> {

    Optional<ExchangeEntity> findByName(String name);
}
