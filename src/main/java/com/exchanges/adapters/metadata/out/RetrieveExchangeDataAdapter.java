package com.exchanges.adapters.metadata.out;

import com.exchanges.application.port.out.RetrieveExchangeDataPort;
import com.exchanges.domain.metadata.Exchange;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class RetrieveExchangeDataAdapter implements RetrieveExchangeDataPort {

    private final ExchangesRepository exchangesRepository;

    @Override
    public Optional<Exchange> retrieveExchangeData(String name) {
        var exchangeEntityOpt = exchangesRepository.findByName(name);
        return exchangeEntityOpt.map(ExchangeEntity::toExchangeModel);
    }
}
