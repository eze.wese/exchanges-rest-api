package com.exchanges.adapters.metadata.out;

import com.exchanges.application.port.out.RetrieveExchangeMetadataPort;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class RetrieveExchangeMetadataAdapter implements RetrieveExchangeMetadataPort {

    private final MetadataRepository metadataRepository;
    private final ObjectMapper mapper;

    @Override
    public Optional<String> retrieveExchangeMetadata(String exchangeId) throws JsonProcessingException {
        var metadataEntityOptional = metadataRepository.findById(exchangeId);
        if (metadataEntityOptional.isPresent()) {
            return Optional.of(mapper.writeValueAsString(metadataEntityOptional.get().getMetadata()));
        }
        return Optional.empty();
    }
}
