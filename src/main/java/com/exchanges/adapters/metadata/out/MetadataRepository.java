package com.exchanges.adapters.metadata.out;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MetadataRepository extends MongoRepository<MetadataEntity, String> {

}
