package com.exchanges.apis.models;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public record OrdersBookApiResponse(
        String symbol,
        List<Bid> bids,
        List<Ask> asks
) {
    public record Bid(
            Double px,
            Double qty,
            BigInteger num
    ) {}

    public record Ask(
            Double px,
            Double qty,
            BigInteger num
    ) {}
}
