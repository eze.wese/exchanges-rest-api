package com.exchanges.apis;

import com.exchanges.configuration.ServiceProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public abstract class BaseApi {

    protected final ServiceProperties serviceProperties;

    protected final ApiClient apiClient;

    protected final ObjectMapper mapper;

    @Autowired
    protected BaseApi(ServiceProperties serviceProperties, ApiClient apiClient, ObjectMapper mapper) {
        this.serviceProperties = serviceProperties;
        this.apiClient = apiClient;
        this.mapper = mapper;
    }
}
