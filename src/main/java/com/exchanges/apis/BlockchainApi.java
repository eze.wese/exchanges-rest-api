package com.exchanges.apis;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.exchanges.apis.models.OrdersBookApiResponse;
import com.exchanges.configuration.ServiceProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class BlockchainApi extends BaseApi {

    public BlockchainApi(ServiceProperties serviceProperties, ApiClient apiClient, ObjectMapper mapper) {
        super(serviceProperties, apiClient, mapper);
    }

    public Map<String, Object> retrieveSymbols() {
        var url = serviceProperties.getBlockchainApiProperties().get("symbolsPath");
        return apiClient.getClient("blockchainApiUrl").get()
                .uri(url)
                .header("accept", "application/json")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<Map<String, Object>>() {})
                .block();
    }

    public OrdersBookApiResponse retrieveOrderBook(String symbol) {
        var url = serviceProperties.getBlockchainApiProperties().get("orderBookPath").replace("{symbol}", symbol);
        return apiClient.getClient("blockchainApiUrl").get()
                .uri(url)
                .header("accept", "application/json")
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<OrdersBookApiResponse>() {})
                .block();
    }
}
