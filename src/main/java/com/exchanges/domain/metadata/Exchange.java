package com.exchanges.domain.metadata;

public record Exchange(
        String id,
        String name
) {
}
