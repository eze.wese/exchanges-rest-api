package com.exchanges.domain.metadata;

public record ResponseMessage(
        String message
) {
}
