package com.exchanges.domain.ordersBook;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Map;

@JsonPropertyOrder({"symbol", "orderTypes"})
public record OrderBooks(
        String symbol,
        @JsonProperty("order_types") Map<String, OrderTypeDetail> orderTypes
) {
    public record OrderTypeDetail(
            @JsonProperty("px_avg") String pxAvg,
            @JsonProperty("qty_avg") String qtyAvg
    ) {}
}
