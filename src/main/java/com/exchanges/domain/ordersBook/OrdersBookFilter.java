package com.exchanges.domain.ordersBook;

public record OrdersBookFilter(
        String symbol,
        String orderType,
        Boolean alphabeticallyOrder
) {
}
