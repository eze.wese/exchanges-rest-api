package com.exchanges.application.port.out;

import com.exchanges.domain.metadata.Exchange;

import java.util.Optional;

public interface RetrieveExchangeDataPort {
    Optional<Exchange> retrieveExchangeData(String name);
}
