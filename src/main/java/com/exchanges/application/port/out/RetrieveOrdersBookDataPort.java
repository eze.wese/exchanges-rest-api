package com.exchanges.application.port.out;

import com.exchanges.domain.ordersBook.OrderBooks;

import java.util.List;

public interface RetrieveOrdersBookDataPort {

    List<OrderBooks> retrieveOrdersBook(List<String> symbols);
}
