package com.exchanges.application.port.out;

import com.exchanges.domain.metadata.Exchange;

public interface SaveExchangeDataPort {
    Exchange saveExchangeData(String id, String name);
}
