package com.exchanges.application.port.out;

import java.util.List;

public interface RetrieveSymbolsPort {
    List<String> retrieveSymbols();
}
