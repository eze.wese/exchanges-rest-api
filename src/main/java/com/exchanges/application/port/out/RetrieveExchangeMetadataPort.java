package com.exchanges.application.port.out;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.Optional;

public interface RetrieveExchangeMetadataPort {
    Optional<String> retrieveExchangeMetadata(String id) throws JsonProcessingException;
}
