package com.exchanges.application.port.out;

import java.util.Map;

public interface SaveMetadataPort {
    void saveMetadata(String id, Map<String, Object> metadata);
}
