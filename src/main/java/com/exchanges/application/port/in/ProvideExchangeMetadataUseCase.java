package com.exchanges.application.port.in;

import com.fasterxml.jackson.core.JsonProcessingException;

public interface ProvideExchangeMetadataUseCase {
    String retrieveExchangeMetadata(String exchangeName) throws JsonProcessingException;
}
