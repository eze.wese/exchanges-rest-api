package com.exchanges.application.port.in;

import com.exchanges.domain.ordersBook.OrderBooks;
import com.exchanges.domain.ordersBook.OrdersBookFilter;

import java.util.List;

public interface ProvideOrdersBookDataUseCase {
    List<OrderBooks> retrieveOrdersBook(OrdersBookFilter filter);
}
