package com.exchanges.application.port.in;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface SaveMetadataUseCase {
    void saveMetadata(MultipartFile multipartFile, String exchangeName) throws IOException;
}
