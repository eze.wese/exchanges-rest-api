package com.exchanges.application.service;

import com.exchanges.application.port.out.RetrieveSymbolsPort;
import com.exchanges.application.port.in.ProvideOrdersBookDataUseCase;
import com.exchanges.application.port.out.RetrieveOrdersBookDataPort;
import com.exchanges.domain.ordersBook.OrderBooks;
import com.exchanges.domain.ordersBook.OrdersBookFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RequiredArgsConstructor
@Component
public class ProvideOrdersBookDataService implements ProvideOrdersBookDataUseCase {

    private final RetrieveSymbolsPort retrieveSymbolsPort;
    private final RetrieveOrdersBookDataPort retrieveOrdersBookDataPort;
    private static final List<String> VALID_ORDER_TYPES = List.of("bid", "ask");

    @Override
    public List<OrderBooks> retrieveOrdersBook(OrdersBookFilter filter) {
        var symbolToFilterOpt = Optional.ofNullable(filter.symbol());
        var orderTypeToFilterOpt = Optional.ofNullable(filter.orderType());
        var alphaOrderOpt = Optional.ofNullable(filter.alphabeticallyOrder());

        var symbols = retrieveSymbolsPort.retrieveSymbols();
        if (symbolToFilterOpt.isPresent() && !symbols.contains(symbolToFilterOpt.get())) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "There are no entries for the specified symbol");
        var ordersBook = retrieveOrdersBookDataPort.retrieveOrdersBook(symbols);
        if (orderTypeToFilterOpt.isPresent()) ordersBook = filterOrdersBookByOrderType(ordersBook, orderTypeToFilterOpt.get());
        if (symbolToFilterOpt.isPresent()) ordersBook = filterOrdersBookBySymbol(ordersBook, symbolToFilterOpt.get());
        if (alphaOrderOpt.isPresent()) ordersBook.sort(Comparator.comparing(OrderBooks::symbol));

        return ordersBook;
    }

    private List<OrderBooks> filterOrdersBookBySymbol(List<OrderBooks> orderBooks, String symbol) {
        return orderBooks.stream().filter(o -> o.symbol().equals(symbol)).toList();
    }

    private List<OrderBooks> filterOrdersBookByOrderType(List<OrderBooks> orderBooks, String orderType) {
        if (!VALID_ORDER_TYPES.contains(orderType)) throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "The order type specified is not valid");
        var ordersBookFiltered = new ArrayList<OrderBooks>();
        orderBooks.forEach(ordersBook -> {
            var orderTypeDetail = ordersBook.orderTypes().get(orderType);
            ordersBookFiltered.add(new OrderBooks(ordersBook.symbol(), Map.of(orderType, orderTypeDetail)));
        });
        return ordersBookFiltered;
    }
}
