package com.exchanges.application.service;

import com.exchanges.application.port.in.SaveMetadataUseCase;
import com.exchanges.application.port.out.SaveExchangeDataPort;
import com.exchanges.application.port.out.SaveMetadataPort;
import com.exchanges.common.CSVHelper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

@RequiredArgsConstructor
@Component
public class SaveMetadataService implements SaveMetadataUseCase {

    private final SaveExchangeDataPort saveExchangeDataPort;
    private final SaveMetadataPort saveMetadataPort;

    @Override
    public void saveMetadata(MultipartFile multipartFile, String exchangeName) throws IOException {
        var exchangeData = saveExchangeDataPort.saveExchangeData(UUID.randomUUID().toString(), exchangeName);
        var csvMap = CSVHelper.csvToMap(multipartFile.getInputStream());
        saveMetadataPort.saveMetadata(exchangeData.id(), csvMap);
    }
}
