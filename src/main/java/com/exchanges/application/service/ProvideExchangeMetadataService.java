package com.exchanges.application.service;

import com.exchanges.application.port.in.ProvideExchangeMetadataUseCase;
import com.exchanges.application.port.out.RetrieveExchangeDataPort;
import com.exchanges.application.port.out.RetrieveExchangeMetadataPort;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@RequiredArgsConstructor
@Component
public class ProvideExchangeMetadataService implements ProvideExchangeMetadataUseCase {

    private final RetrieveExchangeDataPort retrieveExchangeDataPort;
    private final RetrieveExchangeMetadataPort retrieveExchangeMetadataPort;

    @Override
    public String retrieveExchangeMetadata(String exchangeName) throws JsonProcessingException {
        var exchangeDataOpt = retrieveExchangeDataPort.retrieveExchangeData(exchangeName);
        if (exchangeDataOpt.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Metadata for exchange name sent doesn't exist.");
        var metadataOpt = retrieveExchangeMetadataPort.retrieveExchangeMetadata(exchangeDataOpt.get().id());
        if (metadataOpt.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Metadata for exchange name sent doesn't exist.");
        return metadataOpt.get();
    }
}
